#!/usr/bin/zsh
 
() {
    autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
    autoload -z edit-command-line
    autoload compinit
    autoload promptinit
    autoload -U zsh-mime-setup
    autoload -U zsh-mime-handler
}
