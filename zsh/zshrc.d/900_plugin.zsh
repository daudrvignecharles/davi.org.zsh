#!/usr/bin/zsh

() {
    local FILE
    local PLUGIN_PATH=$HOME/.zsh/ressources/plugins/rcs
    for FILE in $(/bin/ls -1 $PLUGIN_PATH) ; do
        if [[ "$FILE[1,3]" = <-> ]] && [[ -h $PLUGIN_PATH/$FILE ]] ; then
            source $PLUGIN_PATH/$FILE
        fi
    done
}
