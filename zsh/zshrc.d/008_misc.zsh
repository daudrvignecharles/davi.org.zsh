#!/usr/bin/zsh

() {
    zle -N edit-command-line
    bindkey "^X^E" edit-command-line
    zsh-mime-setup
    
}
