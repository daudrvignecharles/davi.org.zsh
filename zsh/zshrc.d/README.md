# zshrc.d
### Fonctionnement.
- Contient une liste de script qui seront exécutés par "~/.zsh/zloader.zsh"
- Les fichiers doivent commencer par trois chiffres, le numéro de priorité.
- Tout fichier ne commençant pas par ce numéro de priorité ne sera pas chargé.
- Le plus petit numéro de priorité est 000, le plus grand est 999.
- Les fichiers seront exécuté dans l'ordre numérique croissant de priorité soit de 000 à 999.

### Spécifications des fichiers

#### 1. Nom du script
Le nom du script doit être de la forme suivante :
        
        000_export
        001_prompt
        002_autoload
        003_source
soit le numéro de priorité, un underbar, un nom clair et explicite de préférence.

#### 2. Forme du script
Le script doit avoir la forme suivante :

	()
	{
	    ##########
	    # SCRIPT #
	    ##########
	    return 0
	}

Tout le code exécutable doit être placé dans une fonction anonyme qui doit retourner 0 si elle s'est exécuté correctement.

#### 3. Variables et fonctions

Les variables et fonctions interne au fonctionnement de la fonction anonyme ne doivent pas venir poluer l'espace global. Pour ce faire, un certain nombre de règles doivent être respectées :

- les variables locales doivent être déclarées explicitements :

		typeset MA_VARIABLE_LOCALE
		# ou son équivalent
		local MA_VARIABLE_LOCALE

- les fonctions ne peuvent pas être être déclarées locale en zsh. IL convient donc de les détruires à la fin de l'exécution du script :

		unset -f mafonction
		# ou son équivalent :
		unfonction mafonction

	pour être sur que la fonction soit détruite il est recomandé d'utiliser un bloc "always" mais il est également possible de les détruires après la fonction anonyme :

		()
		{
			{
				function mafonction()
				{
					echo
				}
				# Other code
	  			return 0
			} always {
				unfonction mafonction
			}
		} 