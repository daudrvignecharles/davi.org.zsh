#!/usr/bin/zsh

() {
    {
        local MARKED_PATH
        typeset -a MARKED_PATH
        local PATH
        
        MARKED_PATH=(
            "$HOME/.zsh/ressources/alias"
            "$HOME/.zsh/ressources/extended_alias"
            "$HOME/.zsh/ressources/functions"
        )
        
        load_marked()
        {
            typeset FILE
            typeset SPATH=$1
            for FILE in $(/bin/ls -1 $SPATH) ; do
                EXT=( ${(s/./)FILE} )
                if [[ "$EXT[-2,-1]" == "s zsh" ]] ; then
                    source $SPATH/$FILE
                fi
            done
        }
        
        for PATH in $MARKED_PATH ; do
            [[ -d $PATH ]] && load_marked $PATH
        done
    } always {
        unfunction load_marked
    }
}
