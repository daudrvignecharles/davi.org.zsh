#!/usr/bin/zsh

() {   
    typeset FILE
    typeset RCD_PATH=$HOME/.zsh/zshrc.d
    for FILE in $(/bin/ls -1 $RCD_PATH) ; do
        if [[ "$FILE[1,3]" = <-> ]] ; then
            source $RCD_PATH/$FILE && RET=$?
            if [[ $RET -ne 0 ]] ; then
                printf "[WARNING] The execution of $FILE returned $RET"
            fi
        fi
    done
}

