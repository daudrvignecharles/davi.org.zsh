#!/usr/bin/zsh

aptitude () {
    (( $# == 0 )) && /usr/bin/aptitude && return 0
    case $1 in
        'install'|'update'|'upgrade'|'remove'|'purge'|'clean') sudo /usr/bin/aptitude "$@"
        ;;
        *) /usr/bin/aptitude "$@"
        ;;
    esac
}
