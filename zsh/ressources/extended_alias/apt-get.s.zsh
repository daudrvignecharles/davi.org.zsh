#!/usr/bin/zsh

apt-get () {
    (( $# == 0 )) && /usr/bin/apt-get && return 0
    (( $# == 1 )) && [[ "$1" == "--help" ]] || [[ "$1" == "-h" ]] && /usr/bin/apt-get --help && return 0
    local yesno
    declare -l yesno
    echo -n "\033[1;31mWarning\033[0m : Deprecated on this system, you should use aptitude.\nDo you still launch apt-get ? Y/N "
    read -k1 -s yesno && echo "\n"
    [[ "$yesno" == 'y' ]] && sudo apt-get "$@" || return 0
}
