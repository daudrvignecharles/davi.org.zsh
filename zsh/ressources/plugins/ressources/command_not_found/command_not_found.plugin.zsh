function cnf_preexec() {
    typeset -g cnf_command="${1%% *}"
}

function cnf_precmd() {
    (($?)) && [ -n "$cnf_command" ] && [ -x /usr/share/command-not-found/command-not-found ] && {
        whence -- "$cnf_command" >& /dev/null ||
            /usr/bin/python /usr/share/command-not-found/command-not-found -- "$cnf_command"
        unset cnf_command
    }
}

typeset -ga preexec_functions
typeset -ga precmd_functions
preexec_functions+=cnf_preexec
precmd_functions+=cnf_precmd
