#!/usr/bin/zsh

() {
    set -x
    local PRIORITY
    local PLUGIN
    local RC_NAME
    PRIORITY=$1
    PLUGIN=$2
    if [[ $# != 2 ]] ; then
        printf "Invalid number of arguments."
        return 2
    fi
    if [[ -f $PLUGIN ]] && [[ $1 = <-> ]] ; then
        RC_NAME=$PRIORITY
        RC_NAME+=_${PLUGIN:t}
        ln -sr ./$PLUGIN ./rcs/$RC_NAME
    fi
} "$@"
