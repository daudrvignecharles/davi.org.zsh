#!/usr/bin/zsh

() {
    alias cdd='cd ~/sync.save/programation/projets'
    alias cdz='cd ~/.zsh'
    alias cdp='cd ~/sync.save/programation/python'
    alias cdjango='cd ~/sync.save/programation/python/django/'
}
