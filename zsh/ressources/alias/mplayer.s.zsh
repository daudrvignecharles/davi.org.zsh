#!/usr/bin/zsh

() {
    alias mplay-noscale='mplayer -vo fbdev2'
    alias mplay='mplayer -vo fbdev2 -vf scale=1366:768'
}
