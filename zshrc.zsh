#!/usr/bin/zsh
###########################################################################
#                       _________  _   _ ____   ____                      #
#                      |__  / ___|| | | |  _ \ / ___|                     #
#                        / /\___ \| |_| | |_) | |                         #
#                     _ / /_ ___) |  _  |  _ <| |___                      #
#                    (_)____|____/|_| |_|_| \_\\____|                     #
#                                                                         #
#        Daudré-Vignier Charles <daudre.vignier.charles@narod.ru>         #
#                                                                         #
###########################################################################
############################  Mon zshrc perso  ############################
###########################################################################
#                                                                         #
# Authors : Daudré-Vignier Charles <daudre.vignier.charles@narod.ru>      #
# More on my github : https://github.com/DaudrVignieCharles/zsh-project   #
#                                                                         #
# Copyright 2016 Charles Daudré-Vignier <daudre.vignier.charles@narod.ru> #
# License : GPLv3                                                         #
#                                                                         #
###########################################################################

() {
    source $HOME/.zsh/login_msg.zsh quiet
    source $HOME/.zsh/zloader.zsh
}
