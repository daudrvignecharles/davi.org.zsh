#!/usr/bin/zsh

() {
    () {
        local COMPILE_PATH
        typeset -a COMPILE_PATH
        local PATH
        local FILE

        COMPILE_PATH=(
            "$PWD/zsh/ressources/autoload"
        )
        zcompile $PWD/zshrc.zsh

        for PATH in $COMPILE_PATH ; do
            for FILE in $(/bin/ls -1 $PATH); do
                if [[ ${FILE:e} == "zsh" ]] ; then
                    zcompile $PATH/$FILE
                fi
            done
        done
    }
    rsync -v $PWD/zshrc.zsh $HOME/.zshrc
    rsync -v $PWD/zshrc.zsh.zwc $HOME/.zshrc.zwc
    rsync -vr --links $PWD/zsh/ $HOME/.zsh --delete-after
}
